class BlogCommentsController < ApplicationController
	def index
	end

	def create
		BlogComment.create(blog_comment_params)
		redirect_to blog_path(Blog.find(params[:blog_comment][:blog_id]))
	end

	private

	def blog_comment_params
		params.require(:blog_comment).permit!
	end
end

