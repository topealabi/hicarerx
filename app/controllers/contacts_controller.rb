class ContactsController < ApplicationController
  #before_action :set_contact, only: [:show, :edit, :update, :destroy]
  # skip_before_filter :verify_authenticity_token, :only => [:ipn_notification]
  respond_to :json
  def create

    @contact = Contact.create(contact_params)
    

    @name = params[:name]
    @email = params[:email]
    @message = params[:message]

    @contact.save

    respond_to do |format|
      if @contact.save
      	ContactMailer.contact_mail(@contact.name, @contact.email, @contact.message).deliver
        format.html { redirect_to :back, notice: 'Message sent' }
        #format.json { render action: 'show', status: :created, location: @contact }
        format.json { redirect_to :back, notice: 'Message sent' }
      else
        format.html { render action: 'new' }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit! 
    end
end
