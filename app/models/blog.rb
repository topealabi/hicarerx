class Blog < ActiveRecord::Base
	mount_uploader :image, ImageUploader
	has_many :blog_comments
	accepts_nested_attributes_for :blog_comments

	acts_as_taggable # Alias for acts_as_taggable_on :tags
  	
end
