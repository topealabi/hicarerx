Rails.application.routes.draw do
  
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'tags/:tag', to: 'blogs#index', as: :tag
  resources :blogs
  
  resources :blog_comments

  root 'home#index'
   #resources :contacts
  match '/contacts', to: 'contacts#create', via: [:get, :post]
end
